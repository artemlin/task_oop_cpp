#pragma once
#include <iostream>

template <class T>
struct Item
{
    T m_data;
    Item<T> * m_next = nullptr;
    Item<T> * m_prev = nullptr;
};

template <class T>
class Container
{
private:
    Item<T> * m_begin;
    Item<T> * m_end;
    int m_count;

public:
    Container()
    {
        m_begin = nullptr;
        m_end = nullptr;
        m_count = 0;
    }

    void addBegin(const T & input)
    {
        Item<T> * var = new Item<T>;
        var->m_data = input;
        var->m_prev = nullptr;
        var->m_next = m_begin;

        if (m_count > 0)
        {
            m_begin->m_prev = var;
            m_begin = var;
        }
        else
        {
            m_begin = var;
            m_end = var;
        }

        m_count++;
    }

    void addEnd(const T & input)
    {
        Item<T> * var = new Item<T>;
        var->m_next = nullptr;
        var->m_prev = m_end;
        var->m_data = input;

        if (m_count > 0)
        {
            m_end->m_next = var;
            m_end = var;
        }
        else
        {
            m_begin = var;
            m_end = var;
        }

        m_count++;
    }

    void deleteBegin()
    {
        if (m_count == 0)
        {
            return;
        }

        Item<T> * item = m_begin;

        if (m_count == 1)
        {
            m_begin = nullptr;
            m_end = nullptr;
        }
        else if (m_count > 1)
        {
            Item<T> * itemNext = item->m_next;
            m_begin = itemNext;
            m_begin->m_prev = nullptr;
        }
        delete item;
        m_count--;
    }

    void deleteEnd()
    {
        if (m_count == 0)
        {
            return;
        }

        Item<T> * item = m_end;
        Item<T> * itemPrev = m_end->m_prev;

        if (m_count == 1)
        {
            m_begin = nullptr;
            m_end = nullptr;
        }
        else if (m_count > 1)
        {
            m_end = itemPrev;
            itemPrev->m_next = nullptr;
        }

        delete item;
        m_count--;
    }

    const T getBeginData() const
    {
        if (m_count == 0)
        {
            throw "Empty list. Unable to get data at the begining of the container.";
        }

        Item<T>* var = m_begin;
        return var->m_data;
    }

    const T getEndData() const
    {
        if (m_count == 0)
        {
            throw "Empty list. Unable to get data at the end of the container.";
        }

        Item<T>* var = m_end;
        return var->m_data;
    }

    const Item<T> * getBegin() const
    {
        return m_begin;
    }

    const Item<T> * getEnd() const
    {
        return m_end;
    }

    T pop()
    {
        if (m_count == 0)
        {
            throw "Empty list. Unable to pop.";
        }

        T var = m_end->m_data;
        this->deleteEnd();
        return var;
    }

    int getCount() const 
    {
        return m_count;
    }

    bool isEmpty() const
    {
        if (m_count > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void swap(Container<T> * list)
    {
        Item<T> * save_a = m_begin;
        Item<T> * save_b = m_end;
        int n = list->m_count;
        m_begin = list->m_begin;
        m_end = list->m_end;
        list->m_begin = save_a;
        list->m_end = save_b;
        list->m_count = m_count;
        m_count = n;
    }

    void clear()
    {
        int n = m_count;
        for (int i = 0; i < n; i++)
        {
            deleteBegin();
        }
    }

    void reverse()
    {
        Item<T> * current = m_begin;
        Item<T> * prev = nullptr;
        Item<T> * next = nullptr;
        m_end = m_begin;
        while (current != nullptr)
        {
            next = current->m_next;
            current->m_next = prev;
            current->m_prev = next;
            prev = current;
            current = next;
        }
        m_begin = prev;
    }

    ~Container()
    {
        clear();
    }
};



