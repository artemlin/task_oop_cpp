#include <iostream>
#include "Container.h"
#include "Object.h"


int Object::m_count = 0;

int main()
{
    Container<int> list_1;
    int a = 1;
    list_1.addBegin(a);
    int b = 2;
    list_1.addBegin(b);
    Container<int> list_2;
    list_2.addBegin(b);
    list_2.addBegin(a);

    for (const Item<int>* t = list_1.getBegin(); t != nullptr; t = t->m_next)
    {
        std::cout << t->m_data << std::endl;
    }

    list_2.swap(&list_1);
    for (const Item<int>* t = list_1.getBegin(); t != nullptr; t = t->m_next)
    {
       std::cout << t->m_data << std::endl;
    }

    list_1.reverse();
    list_1.addEnd(10);
    for (const Item<int>* t = list_1.getBegin(); t != nullptr; t = t->m_next)
    {
        std::cout << t->m_data << std::endl;
    }

    list_1.deleteBegin();
    for (const Item<int>* t = list_1.getBegin(); t != nullptr; t = t->m_next)
    {
        std::cout << t->m_data << std::endl;
    }

    Container<Task*> test;
    Container<std::string> results;
    AriphmeticTask * a1 = new AriphmeticTask(1, 2, '*');
    test.addBegin(a1);
    AriphmeticTask * a2 = new AriphmeticTask(4, 5, '+');
    CountObjects<Task*> * a3 = new CountObjects<Task*>(test);
    test.addBegin(a3);
    CountTasksWithResult* a4 = new CountTasksWithResult(test);
    test.addBegin(a4);
    AddTask* a5 = new AddTask(*a2, test);
    test.addBegin(a5);
    ClearObjects<int> * a6 = new ClearObjects<int>(list_1);
    test.addBegin(a6);
    CountObjects<Task*>* a7 = new CountObjects<Task*>(test);
    test.addBegin(a7);
    CountObjects<Task*>* a8 = new CountObjects<Task*>(test);
    test.addBegin(a8);
    CountTasksWithResult* a9 = new CountTasksWithResult(test);
    test.addBegin(a9);
    ExistObjects* a10 = new ExistObjects();
    test.addBegin(a10);
    ClearObjects<int>* a11 = new ClearObjects<int>(list_2);
    test.addBegin(a11);
    CountTasksWithResult* a12 = new CountTasksWithResult(test);
    test.addBegin(a12);
    std::cout << "Tasks in the container: " << test.getCount() << std::endl;
    for (const Item<Task*> * t = test.getEnd(); t != nullptr; t = t->m_prev)
    {
        t->m_data->process();
    }
    int k = test.getCount();
    for (int t = 0; t < k; t++)
    {
        try
        {
            results.addEnd(test.pop()->toString());
        }
        catch (const char* error)
        {
            std::cerr << error << std::endl;
        }
    }
    k = results.getCount();
    for (int t = 0; t < k; t++)
    {
        try
        {
            std::cout << results.pop() << std::endl;
        }
        catch (const char* error)
        {
            std::cerr << error << std::endl;
        }
    }

    std::cout << Object::getCountObjects() << std::endl;
    test.clear();
    results.clear();
    delete a1;
    delete a2;
    delete a3;
    delete a4;
    delete a5;
    delete a6;
    delete a7;
    delete a8;
    delete a9;
    delete a10;
    delete a11;
    delete a12;
    std::cout << Object::getCountObjects() << std::endl;
}
