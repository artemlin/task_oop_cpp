#pragma once
#include <string>
#include "Container.h"

class Object
{
public:
	virtual std::string toString() const = 0;

	Object() 
	{ 
		m_count++;
	}

	~Object() 
	{ 
		m_count--;
	}

	static int getCountObjects()
	{
		return m_count;
	}

private:
	static int m_count;
};


class Task : public Object
{
public:
	Task()
	{
		m_formulation = "Task.";
	}

	virtual std::string toString() const override
	{
		return m_formulation;
	}

	virtual void process() {};

	void completeWithResult(const double result)
	{
		m_result = result;
		m_has_result = true;
		m_is_complete = true;
	}

	bool m_has_result = false;

protected:
	bool m_is_complete = false;
	double m_result = 0;
	std::string m_formulation;
};

class Named : public Object
{
public:
	Named() {}

	Named(const std::string & name) : m_name(name) {}

	void setName(const std::string& name) 
	{
		m_name = name;
	};

	std::string getName() const 
	{
		return m_name;
	};

	virtual std::string toString() const override
	{
		return m_name;
	}

private:
	std::string m_name;
};


class AriphmeticTask : public Task, public Named
{
public:
	AriphmeticTask(double a, double b, char operation) : m_a(a), m_b(b), m_operation(operation)
	{
		m_formulation = "Calculate the value of an arithmetic expression.";
	}

	void process() override 
	{
		if (!m_is_complete)
		{
			try {
				switch (m_operation)
				{
				case '+':
					completeWithResult(m_a + m_b);
					formulateResult();
					break;

				case '-':
					completeWithResult(m_a - m_b);
					formulateResult();
					break;

				case '*':
					completeWithResult(m_a * m_b);
					formulateResult();
					break;

				case '/':
					if (m_b != 0)
					{
						completeWithResult(m_a / m_b);
						formulateResult();
						break;
					}
					else {
						throw "Devision by zero.";
					}

				default:
					throw "Empty operator.";
				}
			}
			catch (const char* error)
			{
				std::cout << error << std::endl;
			}
		}
	}

	virtual std::string toString() const override
	{
		return m_formulation;
	}

private:
	double m_a;
	double m_b;
	char m_operation;

	void formulateResult()
	{
		m_formulation = "The result of the arithmetic expression is " + std::to_string(m_result);
	}
};


class AddTask : public Task
{
public:
	AddTask(Task & task, Container<Task*> & list) : m_task(task), m_container(list)
	{
		m_formulation = "The task of adding a task to a container.";
	}

	void process()override 
	{
		if (!m_is_complete)
		{
			m_container.addEnd(&m_task);
			m_formulation = "The task has been added to the container.";
			m_is_complete = true;
		}
	}

	std::string toString() const override
	{
		return m_formulation;
	}


private:
	Task & m_task;
	Container<Task*> & m_container;
};


template <class T>
class CountObjects : public Task
{
public:
	CountObjects(const Container<T> & list) : m_container(list)
	{
		m_formulation = "The task of counting the number of objects in a container.";
	}

	void process() override
	{
		if (!m_is_complete)
		{
			completeWithResult(m_container.getCount());
			m_formulation = "The number of objects in the container is " + std::to_string(m_result);
		}
	}


	virtual std::string toString() const override
	{
		return m_formulation;
	}

private:
	const Container<typename T> & m_container;
};

class CountTasksWithResult : public Task
{
public:
	CountTasksWithResult(const Container<Task*> & list): m_container(list)
	{
		m_formulation = "The task of counting the number of tasks with a result.";
	}

	void process() override
	{
		if (!m_is_complete)
		{
			for (const Item<Task*> * t = m_container.getBegin(); t != nullptr; t = t->m_next)
			{
				if (t->m_data->m_has_result)
				{
					m_result += 1;
				}
			}
			m_is_complete = true;
			m_formulation = "The number of tasks with a result in the container is " + std::to_string(m_result);
		}
	}

	virtual std::string toString() const override
	{
		return m_formulation;
	}

private:
	const Container<Task*> & m_container;
};


template <class T>
class ClearObjects : public Task
{
public:
	ClearObjects(Container<T> & list) : m_container(list)
	{
		m_formulation = "The task of clearing objects from a container.";
	}

	void process() override
	{
		if (!m_is_complete)
		{
			m_container.clear();
			m_is_complete = true;
			m_formulation = "The container has been cleared.";
		}
	}

	virtual std::string toString() const override
	{
		return m_formulation;
	}

private:
	Container<T> & m_container;
};


class ExistObjects : public Task
{
public:
	ExistObjects()
	{
		m_formulation = "The task of counting the number of created instances of the Object class.";
	}

	void process() override
	{
		if (!m_is_complete)
		{
			completeWithResult(Object::getCountObjects());
			m_formulation = "The number of created instances of the Object class is " + std::to_string(m_result);
		}
	}

	virtual std::string toString() const override
	{
		return m_formulation;
	}
};
